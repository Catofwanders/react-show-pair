import React, {useState, useEffect} from 'react';
import './App.css';

function App() {


  const [cards, setCards] = useState([]);
  const [prevClick, setPrevClick] = useState(null);
  const [counter, setCounter] = useState(1);

  useEffect(()=>{
    createCards();
    setInterval(() => {
      console.log(cards);
      // clearActive();
    }, 1000);
  }, []);

  function setActiveHandler(id) {
    // Если кликнули меньше чем по двум карточкам
    if(counter <= 2){
      const current = cards.map(card => {
        let data = {
          id: card.id,
          position: card.position,
          data_pair: card.data_pair,
        }
        // Находим карточку по которой кликнули
        if(card.matched !== true){
          if(card.id === id){
            // Если это первый клик запоминаем id карточки по которой кликнули
            if(counter === 1){
              setPrevClick(card.id);
            }
            return {
              ...data,
              active: true,
              matched: prevClick !== null && prevClick === card.data_pair ? true : false
            }
          // Если это не первый клик  и data_pair равно текущему id ставим предыдущей карточке статус matched 
          }else if(prevClick === card.id && card.data_pair === id){
            return {
              ...data,
              active: true,
              matched: true
            }
          }
        }
        return card;
      });
      setCards(current);
    }
  }

  function clearActive() {
    const current = cards.map(card => {
      let data = {
        id: card.id,
        position: card.position,
        data_pair: card.data_pair,
        active: false,
        matched: false
      }
      if(card.matched !== true){
        return {
          ...data,
        }
      }
      return card;
    });
    setCards(current);
  }

  function gameLogic(id){
    if(counter >= 2) {
      setCounter(1);
      setPrevClick(null);
    }else {
      setCounter(counter + 1);
    }
    setActiveHandler(id);
  }
  function createCards() {
    const cards_json = [];
    const arr = [...Array(16).keys()];
    arr.map((card, index) => {
      let cardname = index;
      return cards_json[cardname] = {
        id: index + 1,
        position: index + 1,
        data_pair: index %2 === 0 ? index + 2 : index,
        active: false,
        matched: false
      };
    });
    setCards(cards_json);
  }
  
  return (
    <div className="App">
      <div className="gameTable">
       {cards.map(({id, position, data_pair, active, matched})=>(
          <div key={id} className="gameTable__card" onClick={()=>gameLogic(id)}>
            <p>id: {id}</p>
            <p>position: {position}</p>
            <p>pair: {data_pair}</p>
            <p>active: {active.toString()}</p>
            <p>matched: {matched.toString()}</p>
          </div>
        ))}
        {/* {console.log({firstClicked, clickCount})} */}
      </div>
    </div>
  );
}

export default App;
